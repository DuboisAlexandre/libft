#* ************************************************************************** *#
#*                                                                            *#
#*                                                        :::      ::::::::   *#
#*   Makefile                                           :+:      :+:    :+:   *#
#*                                                    +:+ +:+         +:+     *#
#*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        *#
#*                                                +#+#+#+#+#+   +#+           *#
#*   Created: 2016/05/12 11:41:14 by adubois           #+#    #+#             *#
#*   Updated: 2016/05/17 21:09:18 by adubois          ###   ########.fr       *#
#*                                                                            *#
#* ************************************************************************** *#

NAME = libft.a
CC = clang
CFLAGS = -c -Wall -Wextra -Werror -pedantic -O2 -g

INC = libft.h
INCPATH = inc/
INCFLAGS = $(addprefix -I, $(INCPATH))
INCLUDES = $(addprefix $(INCPATH), $(INC))

SRC =	ft_memset.c \
		ft_bzero.c \
		ft_memcpy.c \
		ft_memccpy.c \
		ft_memmove.c \

SRCPATH = src/
SOURCES = $(addprefix $(SRCPATH), $(SRC))

OBJ = $(patsubst %.c,%.o,$(SRC))
OBJPATH = obj/
OBJECTS = $(addprefix $(OBJPATH), $(OBJ))

all: $(NAME)

$(NAME): $(OBJECTS)
	ar rc -o $(NAME) $(OBJECTS)
	ranlib $(NAME)

$(OBJPATH)%.o: $(SRCPATH)%.c $(INCLUDES)
	@if [ ! -d "$(OBJPATH)" ]; then mkdir $(OBJPATH); fi
	$(CC) $(CFLAGS) $(INCFLAGS) -o $@ $<

clean:
	rm -rf $(OBJPATH)

fclean: clean
	rm -rf $(NAME)

re: fclean all

.PHONY: all clean fclean re
